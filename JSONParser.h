#pragma once

#include <memory>
#include <string>
#include <istream>

namespace JSONParser {
    // Class to be extended by clients of the Parser to receive callbacks during
    // the parse
    class ParserCallback {
    protected:
        virtual ~ParserCallback() {}

    public:
        virtual int onDictionaryStart() = 0;
        virtual int onDictionaryEnd() = 0;
        virtual int onDictionaryKey(const std::string& key) = 0;
        virtual int onArrayStart() = 0;
        virtual int onArrayEnd() = 0;
        virtual int onInteger(long long integer) = 0;
    };

    class Parser {
    public:
        Parser(ParserCallback* callbacks);
        ~Parser();

        // Parses JSON from the stream. This will only look for very limited forms of JSON, recognising
        // only dictionaries, arrays and unsigned integers. Anything other than this will cause the
        // parse to fail.
        // Callbacks will be issued when these are encoutered, if any of these callbacks returns non-zero,
        // then the parse will also fail. The failure code will then be returned by the parseStream call.
        // -1 from this call indicates that the parse failed. In that case the error string can be
        // read from the getString method.
        int parseStream(std::istream& inStream);

        // Returns a string describing the error that occured, or empty string if no error occured
        std::string getError();

        struct ParserImpl;
    private:
        std::shared_ptr<ParserImpl> pImpl;
    };
}