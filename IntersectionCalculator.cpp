#include "IntersectionCalculator.h"

#include "Rectangle.h"

#include <algorithm>
#include <iterator>
#include <unordered_set>

// Returns all of the first order intersections, that is the intersections between
// any two of the input rectangles
static RectangleListPtr getFirstOrderIntersections(const RectangleListPtr& input) {
    RectangleListPtr toCheckQueue(new RectangleList(input->begin(), input->end()));
    RectangleListPtr result(new RectangleList());

    while (!toCheckQueue->empty()) {
        auto nextRectangle = toCheckQueue->front();
        toCheckQueue->pop_front();

        for (auto it = toCheckQueue->begin(); it != toCheckQueue->end(); ++it) {
            auto intersection = (*it)->getIntersection(nextRectangle);
            if (intersection != NULL) {
                result->push_back(intersection);
            }
        }
    }

    return result;
}

// Given a set of first order intersections, returns the remaining sets of intersections
// This will perform the comparisons as the first order function, but will ignore
// any results where the ancestors has already been dealt with, to remove duplicates.
// It will continue to make these comparisons, untils all results have been found.
static RectangleListPtr getHigherOrderIntersections(const RectangleListPtr& input) {
    RectangleListPtr toCheckQueue(new RectangleList(input->begin(), input->end()));
    RectangleListPtr result(new RectangleList());

    // If we find two intersections with the same set of ancestors, it is a duplicate
    // and should be ignored. We keep track of those we've found already using an
    // unordered_set 
    std::unordered_set<RectanglePtr, AncestorHash, AncestorEqual> covered;

    auto currentInput = input;
    while (!currentInput->empty()) {
        RectangleListPtr toCheckQueue(new RectangleList(currentInput->begin(), currentInput->end()));
        RectangleListPtr newResults(new RectangleList());

        while (!toCheckQueue->empty()) {
            auto nextRectangle = toCheckQueue->front();
            toCheckQueue->pop_front();

            for (auto it = toCheckQueue->begin(); it != toCheckQueue->end(); ++it) {
                auto intersection = (*it)->getIntersection(nextRectangle);
                if (intersection != NULL && covered.find(intersection) == covered.end()) {
                    newResults->push_back(intersection);
                    covered.insert(intersection);
                }
            }
        }

        result->insert(result->end(), newResults->begin(), newResults->end());
        currentInput = newResults;
    }

    return result;
}

RectangleListPtr IntersectionCalculator::calculateIntersections(const RectangleListPtr& input) {
    RectangleListPtr result(new RectangleList());

    bool done = false;
    auto firstOrder = getFirstOrderIntersections(input);
    result->insert(result->end(), firstOrder->begin(), firstOrder->end());

    auto higherOrder = getHigherOrderIntersections(firstOrder);
    result->insert(result->end(), higherOrder->begin(), higherOrder->end());

    return result;
}