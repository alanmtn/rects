#pragma once

#include <iostream>
#include <memory>
#include <list>

class Rectangle;
typedef std::shared_ptr<const Rectangle> RectanglePtr;
typedef std::list<RectanglePtr> RectangleList;
typedef std::shared_ptr<RectangleList> RectangleListPtr;

namespace InputBuilder {
    // Generates a list of rectangles based on the JSON supplied in the
    // input stream. This expects the JSON to be in the form:
    //  {
    //      "rects": [
    //          { "x": 0, "y": 0, "w": 67, "h": 89 },
    //          { "x": 100, "y": 100, "w": 80, "h": 8898 },
    //          ...
    //          { "x": 99909, "y": 99, "w": 23, "h": 100 }
    //      ]
    //  }
    // Only the first 1000 entries in the rects array will be examined, and any
    // additional keys in the dictionaries will be ignored.
    //
    // If there are any problems parsing the JSON, the a diagnostic will be printed to errStream,
    // and this function will return a null pointer.
    RectangleListPtr buildInputFromStream(std::istream& inStream, std::ostream& errStream = std::cerr);
}