#include "Rectangle.h"

#include "gtest/gtest.h"

#include <algorithm>
#include <tuple>
#include <limits.h>

TEST(RectangleTest, Construct_NoCrash) {
    Rectangle one(6u, 1000u, 79u, 99u);
}

TEST(RectangleTest, ConstructPtr_NoCrash) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
}

TEST(RectangleTest, GetDescription) {
    Rectangle one(6u, 1000u, 79u, 99u);

    ASSERT_EQ("(6, 1000), w=79, h=99", one.getDescription());
}

typedef std::tuple<RectanglePtr, RectanglePtr, bool> EqualityTestCaseTuple;

class EqualityTest : public ::testing::TestWithParam<EqualityTestCaseTuple> {
};

TEST_P(EqualityTest, TestEquality) {
    auto res = *(std::get<0>(GetParam())) == *(std::get<1>(GetParam()));
    ASSERT_EQ(std::get<2>(GetParam()), res);
    auto notRes = *(std::get<0>(GetParam())) != *(std::get<1>(GetParam()));
    ASSERT_EQ(!std::get<2>(GetParam()), notRes);
}

#define EQUALITY_TEST_CASE(x1, y1, width1, height1, x2, y2, width2, height2, res) \
    EqualityTestCaseTuple(RectanglePtr(new Rectangle(x1, y1, width1, height1)), RectanglePtr(new Rectangle(x2, y2, width2, height2)), res)

static EqualityTestCaseTuple equalityTestCases[] = {
    EQUALITY_TEST_CASE(0u, 0u, 0u, 0u,   0u, 0u, 0u, 0u,   true),
    EQUALITY_TEST_CASE(1u, 2u, 3u, 4u,   1u, 2u, 3u, 4u,   true),
    EQUALITY_TEST_CASE(1u, 2u, 3u, 4u,   1u, 2u, 3u, 0u,   false),
    EQUALITY_TEST_CASE(1u, 2u, 3u, 4u,   1u, 2u, 0u, 4u,   false),
    EQUALITY_TEST_CASE(1u, 2u, 3u, 4u,   1u, 0u, 3u, 4u,   false),
    EQUALITY_TEST_CASE(1u, 2u, 3u, 4u,   0u, 2u, 3u, 4u,   false),
    EQUALITY_TEST_CASE(1u, 2u, 3u, 4u,   5u, 6u, 7u, 8u,   false)
};

INSTANTIATE_TEST_CASE_P(RectangleTest, EqualityTest, ::testing::ValuesIn(equalityTestCases));

TEST(RectangleTest, IntersectionWithNull_ReturnsNull) {
    Rectangle rect(7u, 7u, 7u, 7u);
    auto res = rect.getIntersection(RectanglePtr());
    ASSERT_TRUE(res == NULL);
}

typedef std::tuple<RectanglePtr, RectanglePtr, RectanglePtr> IntersectionTestCaseTuple;

class IntersectionTest : public ::testing::TestWithParam<IntersectionTestCaseTuple> {
};

TEST_P(IntersectionTest, TestIntersection) {
    auto res = std::get<0>(GetParam())->getIntersection(std::get<1>(GetParam()));

    if (std::get<2>(GetParam()) == NULL) {
        ASSERT_TRUE(res == NULL);
    } else {
        ASSERT_EQ(*(std::get<2>(GetParam())), *res);
    }
}

TEST_P(IntersectionTest, TestReverse) {
    // Should get the smae result when the inputs are reversed
    auto res = std::get<1>(GetParam())->getIntersection(std::get<0>(GetParam()));

    if (std::get<2>(GetParam()) == NULL) {
        ASSERT_TRUE(res == NULL);
    } else {
        ASSERT_EQ(*(std::get<2>(GetParam())), *res);
    }
}

#define INTERSECTION_TEST_CASE_MISS(x1, y1, width1, height1, x2, y2, width2, height2) \
    IntersectionTestCaseTuple(RectanglePtr(new Rectangle(x1, y1, width1, height1)), RectanglePtr(new Rectangle(x2, y2, width2, height2)), RectanglePtr())

#define INTERSECTION_TEST_CASE_HIT(x1, y1, width1, height1, x2, y2, width2, height2, xRes, yRes, widthRes, heightRes) \
    IntersectionTestCaseTuple(RectanglePtr(new Rectangle(x1, y1, width1, height1)), RectanglePtr(new Rectangle(x2, y2, width2, height2)), RectanglePtr(new Rectangle(xRes, yRes, widthRes, heightRes)))

static IntersectionTestCaseTuple intersectionTestCases[] = {
    INTERSECTION_TEST_CASE_HIT(0u, 0u, 0u, 0u,   0u, 0u, 0u, 0u,   0u, 0u, 0u, 0u),
    INTERSECTION_TEST_CASE_HIT(0u, 0u, 10u, 10u,   0u, 0u, 10u, 10u,   0u, 0u, 10u, 10u),
    INTERSECTION_TEST_CASE_HIT(67u, 67u, 0u, 0u,   67u, 67u, 0u, 0u,   67u, 67u, 0u, 0u),
    INTERSECTION_TEST_CASE_HIT(67u, 67u, 90u, 90u,   67u, 67u, 90u, 90u,   67u, 67u, 90u, 90u),
    
    INTERSECTION_TEST_CASE_HIT(UINT_MAX, UINT_MAX, 90u, 90u,   UINT_MAX, UINT_MAX, 90u, 90u,   UINT_MAX, UINT_MAX, 90u, 90u),
    INTERSECTION_TEST_CASE_HIT(67u, 67u, UINT_MAX, UINT_MAX,   67u, 67u, UINT_MAX, UINT_MAX,   67u, 67u, UINT_MAX, UINT_MAX),
    INTERSECTION_TEST_CASE_HIT(UINT_MAX, UINT_MAX, UINT_MAX, UINT_MAX,   UINT_MAX, UINT_MAX, UINT_MAX, UINT_MAX,   UINT_MAX, UINT_MAX, UINT_MAX, UINT_MAX),

    INTERSECTION_TEST_CASE_HIT(10u, 10u, 90u, 90u,   20u, 20u, 90u, 90u,   20u, 20u, 80u, 80u),
    INTERSECTION_TEST_CASE_HIT(10u, 10u, 90u, 90u,   20u, 20u, 70u, 70u,   20u, 20u, 70u, 70u),
    INTERSECTION_TEST_CASE_HIT(10u, 10u, 90u, 90u,   20u, 20u, 70u, 90u,   20u, 20u, 70u, 80u),
    INTERSECTION_TEST_CASE_HIT(10u, 10u, 90u, 90u,   20u, 20u, 90u, 70u,   20u, 20u, 80u, 70u),

    INTERSECTION_TEST_CASE_MISS(10u, 10u, 90u, 90u,   110u, 110u, 80u, 80u),
    INTERSECTION_TEST_CASE_MISS(10u, 10u, 90u, 90u,   10u, 110u, 80u, 80u),
    INTERSECTION_TEST_CASE_MISS(10u, 10u, 90u, 90u,   110u, 10u, 80u, 80u),

    // While no rectangle can be described whose x and y value is greater than UINT_MAX,
    // it is possible for the right and bottom edges of the rectangle to be beyond UINT_MAX
    // We need to ensure that any wraparounds in the calculations don't invalidate the intersection produced, 
    // which will always be describable
    INTERSECTION_TEST_CASE_HIT(UINT_MAX - 100u, UINT_MAX - 100u, 190u, 190u,   UINT_MAX - 20u, UINT_MAX - 20u, 90u, 70u,   UINT_MAX - 20u, UINT_MAX - 20u, 90u, 70u),
    INTERSECTION_TEST_CASE_HIT(180u, 180u, UINT_MAX - 100u, UINT_MAX - 100u,   190u, 190u, UINT_MAX - 20u, UINT_MAX - 20u,   190u, 190u, UINT_MAX - 110u, UINT_MAX - 110u),
    INTERSECTION_TEST_CASE_HIT(UINT_MAX - 100u, UINT_MAX - 100u, 200u, 200u,   UINT_MAX - 50u, UINT_MAX - 50u, 20u, 20u,   UINT_MAX - 50u, UINT_MAX - 50u, 20u, 20u),
    INTERSECTION_TEST_CASE_HIT(UINT_MAX - 100u, UINT_MAX - 100u, 90u, 90u,   UINT_MAX - 50u, UINT_MAX - 50u, 120u, 120u,   UINT_MAX - 50u, UINT_MAX - 50u, 40u, 40u)

};

INSTANTIATE_TEST_CASE_P(RectangleTest, IntersectionTest, ::testing::ValuesIn(intersectionTestCases));

TEST(RectangleTest, getAncestors_NoParent) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));

    ASSERT_EQ(one->getAncestorsBegin(), one->getAncestorsEnd());
}

TEST(RectangleTest, getAncestors_FirstOrderChild) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr two(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr three(new Rectangle(6u, 1000u, 79u, 99u));

    auto child = one->getIntersection(two);

    ASSERT_NE(child->getAncestorsEnd(), std::find(child->getAncestorsBegin(), child->getAncestorsEnd(), one));
    ASSERT_NE(child->getAncestorsEnd(), std::find(child->getAncestorsBegin(), child->getAncestorsEnd(), two));
    ASSERT_EQ(child->getAncestorsEnd(), std::find(child->getAncestorsBegin(), child->getAncestorsEnd(), three));
}

TEST(RectangleTest, getAncestors_SecondOrderChild) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr two(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr three(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr four(new Rectangle(6u, 1000u, 79u, 99u));

    auto childOne = one->getIntersection(two);
    auto childTwo = two->getIntersection(three);
    auto grandChild = childOne->getIntersection(childTwo);

    ASSERT_NE(grandChild->getAncestorsEnd(), std::find(grandChild->getAncestorsBegin(), grandChild->getAncestorsEnd(), one));
    ASSERT_NE(grandChild->getAncestorsEnd(), std::find(grandChild->getAncestorsBegin(), grandChild->getAncestorsEnd(), two));
    ASSERT_NE(grandChild->getAncestorsEnd(), std::find(grandChild->getAncestorsBegin(), grandChild->getAncestorsEnd(), three));
    ASSERT_EQ(grandChild->getAncestorsEnd(), std::find(grandChild->getAncestorsBegin(), grandChild->getAncestorsEnd(), four));
}

TEST(RectangleTest, getAncestorsHash_FirstOrderChild) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr two(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr three(new Rectangle(6u, 1000u, 79u, 99u));

    // These two have the same set of ancestors
    auto childOne = one->getIntersection(two);
    auto childTwo = two->getIntersection(one);

    ASSERT_NE(0u, one->getAncestorsHash());
    ASSERT_EQ(one->getAncestorsHash(), one->getAncestorsHash());
    ASSERT_NE(0u, childOne->getAncestorsHash());
    ASSERT_EQ(childOne->getAncestorsHash(), childTwo->getAncestorsHash());
}

TEST(RectangleTest, getAncestorsHash_SecondOrderChild) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr two(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr three(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr four(new Rectangle(6u, 1000u, 79u, 99u));

    auto childOne = one->getIntersection(two);
    auto childTwo = two->getIntersection(three);
    auto childThree = three->getIntersection(one);
    auto grandChildOne = childOne->getIntersection(childTwo);
    auto grandChildTwo = childTwo->getIntersection(childThree);
    auto grandChildThree = childThree->getIntersection(childOne);

    ASSERT_NE(0u, grandChildOne->getAncestorsHash());
    ASSERT_EQ(grandChildOne->getAncestorsHash(), grandChildOne->getAncestorsHash());
    ASSERT_EQ(grandChildOne->getAncestorsHash(), grandChildTwo->getAncestorsHash());
    ASSERT_EQ(grandChildOne->getAncestorsHash(), grandChildThree->getAncestorsHash());
}

TEST(RectangleTest, ancestorsEqual_Parent) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr two(new Rectangle(6u, 1000u, 79u, 99u));

    ASSERT_FALSE(two->ancestorsEqual(one));
    ASSERT_FALSE(one->ancestorsEqual(two));
}

TEST(RectangleTest, ancestorsEqual_FirstOrderChild) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr two(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr three(new Rectangle(6u, 1000u, 79u, 99u));

    // These two have the same set of ancestors
    auto childOne = one->getIntersection(two);
    auto childTwo = two->getIntersection(one);
    auto childThree = one->getIntersection(three);

    ASSERT_TRUE(childOne->ancestorsEqual(childTwo));
    ASSERT_TRUE(childTwo->ancestorsEqual(childOne));
    ASSERT_FALSE(childOne->ancestorsEqual(childThree));
}

TEST(RectangleTest, ancestorsEqual_SecondOrderChild) {
    RectanglePtr one(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr two(new Rectangle(6u, 1000u, 79u, 99u));
    RectanglePtr three(new Rectangle(6u, 1000u, 79u, 99u));

    auto childOne = one->getIntersection(two);
    auto childTwo = two->getIntersection(one);

    // These two have the same set of ancestors
    auto grandChildOne = childOne->getIntersection(childTwo);
    auto grandChildTwo = childTwo->getIntersection(childOne);
    auto grandChildThree = childOne->getIntersection(three);

    ASSERT_TRUE(grandChildOne->ancestorsEqual(grandChildTwo));
    ASSERT_TRUE(grandChildTwo->ancestorsEqual(grandChildOne));
    ASSERT_FALSE(grandChildOne->ancestorsEqual(grandChildThree));
}