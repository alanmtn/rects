#include "InputBuilder.h"
#include "IntersectionCalculator.h"
#include "Rectangle.h"

#include <fstream>
#include <iostream>
#include <set>
#include <vector>

struct RectangleLess {
    bool operator()(const RectanglePtr& lhs, const RectanglePtr& rhs) const {
        return lhs->getNumber() < rhs->getNumber();
    }
};


int main(int argc, char** argv) {
    if (argc != 2) {
        std::cout << "Usage: rects file-path" << std::endl;
        return 1;
    }

    std::string fileName(argv[1]);

    std::ifstream fileStream;
    fileStream.open(fileName);

    if (fileStream.fail()) {
        std::cerr << "Error: Unable to open file " << fileName << std::endl;
        return 1;
    }

    auto rectangles = InputBuilder::buildInputFromStream(fileStream);

    if (rectangles == NULL) {
        // Error already reported
        return 1;
    }

    std::cout << "Input:" << std::endl;
    for (auto it = rectangles->begin(); it != rectangles->end(); ++it) {
        std::cout << "    " << (*it)->getNumber() << ": Rectangle at " << (*it)->getDescription() << std::endl;
    }

    auto intersections = IntersectionCalculator::calculateIntersections(rectangles);

    std::cout << std::endl << "Intersections" << std::endl;

    // This is probably not the best way to do this. Realistically we would need to consider localisation.
    for (auto it = intersections->begin(); it != intersections->end(); ++it) {
        std::cout << "    Between rectangles ";

        std::set<RectanglePtr, RectangleLess> sortedAncestors((*it)->getAncestorsBegin(), (*it)->getAncestorsEnd());
        std::vector<RectanglePtr> randomAccess(sortedAncestors.begin(), sortedAncestors.end());

        // We can assume the number of ancestors is not 1 or 0
        if (randomAccess.size() > 2) {
            for (size_t i = 0; i < randomAccess.size() - 2; i++) {
                std::cout << randomAccess[i]->getNumber() << ", ";
            }
        }
        std::cout << randomAccess[randomAccess.size() - 2]->getNumber() << " and " << randomAccess[randomAccess.size() - 1]->getNumber();
        std::cout << " at " << (*it)->getDescription() << std::endl;
    }

    return 0;
}