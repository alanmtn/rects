# README #

## Build

I have used CMake as a build tool. The code should compile on MacOS and Linux, but I have only tested it on MacOS. 
Some changes to the link paths would be required to build on Windows, but I do not have a Windows development environment
set up to verify and test these.

### Tool Chain (MacOS)

- CMake version 3.7
- XCode version 8.2
- git version 2.10.1

### Build steps

```shell
git clone https://alanmtn@bitbucket.org/alanmtn/rects.git
mkdir build
cd build
cmake ..
make
```

This will build the rects executable and the rectsTest Unit Tests.

### Libraries used

I have used the following open source libraries, the generated make script will download these automatically from github when
run on a machine with internet access.

- Google Test - Unit test framework (https://github.com/google/googletest).
- YAJL - JSON parser (https://lloyd.github.io/yajl/).

## Tests (MacOS and Linux)

To run the unit tests, after build execute:

```shell
./rectsTest
```

## Executing (MacOS and Linux)

To execute:
```shell
./rects testData.json 
```

Where testData.json is the name of the file containing the JSON description of the rectangles

## A note on performance

While performance was not one of the requirements, I have made some minor improvements over the pure brute-force approach
where it was easy and obvious to do so, for example using hashing in the comparison of ancestors. I can see a range of other
changes that could be made, such as sorting the rectangle list before searching or using some kind of data structure to help the
search. However these all rely upon there being a low ratio of rectangles overlaps. In the worst case scenario, where every
rectangle is the same and so intersects every other one, the problem becomes one of listing all possible combinations of a set,
(n-choose-k for all k) which has an exponential complexity. From my testing, just 12 identical rectangles took in excess of 30
seconds.

