#pragma once

#include <memory>
#include <list>

class Rectangle;
typedef std::shared_ptr<const Rectangle> RectanglePtr;
typedef std::list<RectanglePtr> RectangleList;
typedef std::shared_ptr<RectangleList> RectangleListPtr;

namespace IntersectionCalculator {
    // Calculate all of the intersections between a set of rectangles
    RectangleListPtr calculateIntersections(const RectangleListPtr& rectangles);
};