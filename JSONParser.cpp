#include "JSONParser.h"

#include "yajl/yajl_parse.h"

#include <stack>
#include <vector>

namespace JSONParser {
    static const size_t BUFFER_SIZE = 200;
    
    struct Parser::ParserImpl {
        ParserImpl(ParserCallback* _callbacks) : result(0), callbacks(_callbacks) {}

        std::string errorString;
        int result;
        ParserCallback* callbacks;
    };

    static int cancelOperation(void* yajlCtx) {
        Parser::ParserImpl* parser = reinterpret_cast<Parser::ParserImpl*>(yajlCtx);
        parser->result = -1;
        parser->errorString = "Valid but unacceptable object supplied in JSON";
        return 0;
    }

    static int cancelOperation(void* yajlCtx, int intValue) {
        return cancelOperation(yajlCtx);
    }

    static int cancelOperation(void* yajlCtx, double doubleValue) {
        return cancelOperation(yajlCtx);
    }

    static int cancelOperation(void* yajlCtx, const unsigned char* strValue, size_t strLen) {
        return cancelOperation(yajlCtx);
    }

    static int integerParsed(void* yajlCtx, long long intValue) {
        Parser::ParserImpl* parser = reinterpret_cast<Parser::ParserImpl*>(yajlCtx);
        parser->result = parser->callbacks->onInteger(intValue);
        return (parser->result == 0 ? -1 : 0);
    }

    static int dictionaryStartParsed(void* yajlCtx) {
        Parser::ParserImpl* parser = reinterpret_cast<Parser::ParserImpl*>(yajlCtx);
        parser->result = parser->callbacks->onDictionaryStart() ;
        return (parser->result == 0 ? -1 : 0);
    }

    static int dictionaryKeyParsed(void* yajlCtx, const unsigned char* str, size_t len) {
        Parser::ParserImpl* parser = reinterpret_cast<Parser::ParserImpl*>(yajlCtx);
        std::string key(reinterpret_cast<const char*>(str), len);
        parser->result = parser->callbacks->onDictionaryKey(key);
        return (parser->result == 0 ? -1 : 0);
    }

    static int dictionaryEndParsed(void* yajlCtx) {
        Parser::ParserImpl* parser = reinterpret_cast<Parser::ParserImpl*>(yajlCtx);
        parser->result = parser->callbacks->onDictionaryEnd();
        return (parser->result == 0 ? -1 : 0);
    }

    static int arrayStartParsed(void* yajlCtx) {
        Parser::ParserImpl* parser = reinterpret_cast<Parser::ParserImpl*>(yajlCtx);
        parser->result = parser->callbacks->onArrayStart();
        return (parser->result == 0 ? -1 : 0);
    }

    static int arrayEndParsed(void* yajlCtx) {
        Parser::ParserImpl* parser = reinterpret_cast<Parser::ParserImpl*>(yajlCtx);
        parser->result = parser->callbacks->onArrayEnd();
        return (parser->result == 0 ? -1 : 0);
    }

    class YajlHandle {
    public:
        YajlHandle(const yajl_callbacks* callbacks, void* yajlCtx) {
            handle = yajl_alloc(callbacks, NULL, yajlCtx);
        }

        ~YajlHandle() {
            yajl_free(handle);
        }

        yajl_handle handle;
    };

    Parser::Parser(ParserCallback* callbacks) : pImpl(new ParserImpl(callbacks)) {
    }

    Parser::~Parser() {
    }

    int Parser::parseStream(std::istream& instream) {
        pImpl->result = 0;
        pImpl->errorString = "";
        
        yajl_callbacks callbacks = {
            &cancelOperation, // null
            &cancelOperation, // boolean
            &integerParsed, // integer
            &cancelOperation, // double
            NULL, // string number (left out, this causes a parse error for numbers outside of long long's bounds)
            &cancelOperation, // string
            &dictionaryStartParsed, // dictionary open
            &dictionaryKeyParsed, // dictionary key
            &dictionaryEndParsed, // dictionary close
            &arrayStartParsed, // array start
            &arrayEndParsed // array end
        };
        
        YajlHandle yajlHandle(&callbacks, reinterpret_cast<void*>(pImpl.get()));

        if (yajlHandle.handle == NULL) {
            pImpl->errorString = "Unable to initialise parser";
            pImpl->result = -1;
            return pImpl->result;
        }

        std::vector<char> buffer(BUFFER_SIZE, '\0');

        while (true) {
            // This will stop reading at a end-of-line, end-of-file or after BUFFER_SIZE - 1 chars.
            instream.get(&buffer[0], BUFFER_SIZE);

            if (!instream.eof() && (instream.fail() || instream.bad())) {
                pImpl->result = -1;
                pImpl->errorString = "Unable to read from input stream";
                break;
            }

            // Aside from being ugly, these casts may be a portability problem.
            // Char may be signed or unsigned by default depending on the compiler.
            auto res = yajl_parse(yajlHandle.handle, const_cast<unsigned char*>(reinterpret_cast<const unsigned char*>(&buffer[0])), strlen(&buffer[0]));

            if (res == yajl_status_client_canceled) {
                // Error should have been reported in a callback
                break;
            } else if (res != yajl_status_ok) {
                pImpl->errorString = (pImpl->errorString.empty() ? "Unable to parse JSON supplied" : pImpl->errorString);
                pImpl->result = -1;
                break;
            }

            // Ignore any new-line chars.
            while (instream.peek() == '\r' || instream.peek() == '\n') {
                instream.ignore(1);
            }

            // If we've reached the end of the file, then we are done
            if (instream.eof()) {
                break;
            }
        }

        return pImpl->result;
    }

    std::string Parser::getError() {
        return pImpl->errorString;
    }
}