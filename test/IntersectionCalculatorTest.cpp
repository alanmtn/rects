#include "IntersectionCalculator.h"

#include "Rectangle.h"

#include "gtest/gtest.h"

#include <vector>

TEST(IntersectionCalculatorTest, EmptyInput) {
    RectangleListPtr input(new RectangleList());

    auto result = IntersectionCalculator::calculateIntersections(input);

    ASSERT_EQ(0, result->size());
}

TEST(IntersectionCalculatorTest, SingleRectangle) {
    RectangleListPtr input(new RectangleList());

    input->push_back(RectanglePtr(new Rectangle(0u, 0u, 100u, 100u)));

    auto result = IntersectionCalculator::calculateIntersections(input);

    ASSERT_EQ(0, result->size());
}

TEST(IntersectionCalculatorTest, TwoNonIntersectingRectangles) {
    RectangleListPtr input(new RectangleList());

    input->push_back(RectanglePtr(new Rectangle(0u, 0u, 100u, 100u)));
    input->push_back(RectanglePtr(new Rectangle(1100u, 1100u, 100u, 100u)));

    auto result = IntersectionCalculator::calculateIntersections(input);

    ASSERT_EQ(0, result->size());
}

TEST(IntersectionCalculatorTest, TwoIntersectingRectangles) {
    RectangleListPtr input(new RectangleList());

    input->push_back(RectanglePtr(new Rectangle(0u, 0u, 100u, 100u)));
    input->push_back(RectanglePtr(new Rectangle(50u, 50u, 100u, 100u)));

    auto result = IntersectionCalculator::calculateIntersections(input);

    ASSERT_EQ(1, result->size());

    std::vector<RectanglePtr> resultsVector(result->begin(), result->end());
    ASSERT_EQ(Rectangle(50u, 50u, 50u, 50u), *(resultsVector[0]));
}

TEST(IntersectionCalculatorTest, ThreeIntersectingRectangles_FirstOrder) {
    RectangleListPtr input(new RectangleList());

    input->push_back(RectanglePtr(new Rectangle(0u, 0u, 100u, 100u)));
    input->push_back(RectanglePtr(new Rectangle(50u, 50u, 100u, 100u)));
    input->push_back(RectanglePtr(new Rectangle(125u, 125u, 20u, 20u)));

    auto result = IntersectionCalculator::calculateIntersections(input);

    ASSERT_EQ(2, result->size());

    std::vector<RectanglePtr> resultsVector(result->begin(), result->end());
    ASSERT_EQ(Rectangle(50u, 50u, 50u, 50u), *(resultsVector[0]));
    ASSERT_EQ(Rectangle(125u, 125u, 20u, 20u), *(resultsVector[1]));
}

TEST(IntersectionCalculatorTest, ThreeIntersectingRectangles_SecondOrder) {
    RectangleListPtr input(new RectangleList());

    input->push_back(RectanglePtr(new Rectangle(0u, 0u, 100u, 100u)));
    input->push_back(RectanglePtr(new Rectangle(50u, 50u, 100u, 100u)));
    input->push_back(RectanglePtr(new Rectangle(75u, 75u, 20u, 20u)));

    auto result = IntersectionCalculator::calculateIntersections(input);

    ASSERT_EQ(4, result->size());

    std::vector<RectanglePtr> resultsVector(result->begin(), result->end());
    ASSERT_EQ(Rectangle(50u, 50u, 50u, 50u), *(resultsVector[0]));
    ASSERT_EQ(Rectangle(75, 75u, 20u, 20u), *(resultsVector[1]));
    ASSERT_EQ(Rectangle(75u, 75u, 20u, 20u), *(resultsVector[2]));
    ASSERT_EQ(Rectangle(75u, 75u, 20u, 20u), *(resultsVector[3]));
}

TEST(IntersectionCalculatorTest, FourIntersectingRectangles_ThirdOrder) {
    RectangleListPtr input(new RectangleList());

    input->push_back(RectanglePtr(new Rectangle(0u, 0u, 100u, 100u))); // 1
    input->push_back(RectanglePtr(new Rectangle(50u, 50u, 100u, 100u))); // 2
    input->push_back(RectanglePtr(new Rectangle(75u, 75u, 20u, 20u))); // 3
    input->push_back(RectanglePtr(new Rectangle(76u, 76u, 20u, 20u))); // 4

    auto result = IntersectionCalculator::calculateIntersections(input);

    ASSERT_EQ(11, result->size());

    std::vector<RectanglePtr> resultsVector(result->begin(), result->end());
    ASSERT_EQ(Rectangle(50u, 50u, 50u, 50u), *(resultsVector[0])); // 1 & 2
    ASSERT_EQ(Rectangle(75u, 75u, 20u, 20u), *(resultsVector[1])); // 1 & 3
    ASSERT_EQ(Rectangle(76u, 76u, 20u, 20u), *(resultsVector[2])); // 1 & 4
    ASSERT_EQ(Rectangle(75u, 75u, 20u, 20u), *(resultsVector[3])); // 2 & 3
    ASSERT_EQ(Rectangle(76u, 76u, 20u, 20u), *(resultsVector[4])); // 2 & 4
    ASSERT_EQ(Rectangle(76u, 76u, 19u, 19u), *(resultsVector[5])); // 3 & 4
    ASSERT_EQ(Rectangle(75u, 75u, 20u, 20u), *(resultsVector[6])); // 1 & 2 & 3
    ASSERT_EQ(Rectangle(76u, 76u, 20u, 20u), *(resultsVector[7])); // 1 & 2 & 4
    ASSERT_EQ(Rectangle(76u, 76u, 19u, 19u), *(resultsVector[8])); // 2 & 3 & 4
    ASSERT_EQ(Rectangle(76u, 76u, 19u, 19u), *(resultsVector[9])); // 1 & 3 & 4
    ASSERT_EQ(Rectangle(76, 76u, 19u, 19u), *(resultsVector[10])); // 1 & 2 & 3 & 4
}

TEST(IntersectionCalculatorTest, DISABLED_WorstCaseScenario) {
    RectangleListPtr input(new RectangleList());

    for (size_t i = 0; i < 12; i++) { // Any more than 12 is unfeasible
        input->push_back(RectanglePtr(new Rectangle(0u, 0u, 100u, 100u)));
    }

    auto result = IntersectionCalculator::calculateIntersections(input);
}

