#include "JSONParser.h"

#include "gtest/gtest.h"

#include <climits>
#include <sstream>

class TestParserCallbacks : public JSONParser::ParserCallback {
public:
    TestParserCallbacks() : response(0) {}
    virtual ~TestParserCallbacks() {}

    int onDictionaryStart() override {
        results.push_back("{");
        return response;
    }

    int onDictionaryEnd() override {
        results.push_back("}");
        return response;
    }

    int onDictionaryKey(const std::string& key) override {
        results.push_back(key + ":");
        return response;
    }

    int onArrayStart() override {
        results.push_back("[");
        return response;
    }

    int onArrayEnd() override {
        results.push_back("]");
        return response;
    }

    int onInteger(long long integer) override {
        std::stringstream sstream;
        sstream << integer;
        results.push_back(sstream.str());
        return response;
    }

    int response;

    std::vector<std::string> results;
};

TEST(JSONParserTest, EmptyString) {
    std::istringstream inStream("");
    
    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    // This isn't an error, but there's no JSON to parse
    ASSERT_EQ(0, result);
    ASSERT_EQ(0, callbacks.results.size());
}

TEST(JSONParserTest, WhiteSpace) {
    std::istringstream inStream("  \r\n   \t  \r\n   ");
    
    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    // This isn't an error, but there's no JSON to parse
    ASSERT_EQ(0, result);
    ASSERT_EQ(0, callbacks.results.size());
}

TEST(JSONParserTest, NewLineAtStart) {
    std::istringstream inStream("\r\n  \r\n   \t  \r\n   ");
    
    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    // This isn't an error, but there's no JSON to parse
    ASSERT_EQ(0, result);
    ASSERT_EQ(0, callbacks.results.size());
}

TEST(JSONParserTest, Array) {
    std::istringstream inStream("[ ]");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(0, result);
    std::vector<std::string> expected = {
        "[", "]"
    };
    ASSERT_EQ(expected, callbacks.results);
}

TEST(JSONParserTest, ArrayOfInts) {
    std::istringstream inStream("[ 0, 1, 2, 3, 4 ]");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(0, result);
    std::vector<std::string> expected = {
        "[", "0", "1", "2", "3", "4", "]"
    };
    ASSERT_EQ(expected, callbacks.results);
}

TEST(JSONParserTest, Dictionary) {
    std::istringstream inStream("{ }");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(0, result);
    std::vector<std::string> expected = {
        "{", "}"
    };
    ASSERT_EQ(expected, callbacks.results);
}

TEST(JSONParserTest, DictionaryOfInts) {
    std::istringstream inStream("{ \"one\": 1, \"two\": 2, \"three\": 3 }");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(0, result);
    std::vector<std::string> expected = {
        "{", "one:", "1", "two:", "2", "three:", "3", "}"
    };
    ASSERT_EQ(expected, callbacks.results);
}

TEST(JSONParserTest, MultipleLevels) {
    std::istringstream inStream("{ \"one\": 1, \"two\": [ 5, 7, [ { \"three\": 3 } ] ] }");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(0, result);
    std::vector<std::string> expected = {
        "{", "one:", "1", "two:", "[", "5", "7", "[", "{", "three:", "3", "}", "]", "]", "}"
    };
    ASSERT_EQ(expected, callbacks.results);
}

TEST(JSONParserTest, Float) {
    std::istringstream inStream("[ 10.0 ]");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(-1, result);
    ASSERT_EQ("Valid but unacceptable object supplied in JSON", parser.getError());
}

TEST(JSONParserTest, String) {
    std::istringstream inStream("[ \"string\" ]");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(-1, result);
    ASSERT_EQ("Valid but unacceptable object supplied in JSON", parser.getError());
}

TEST(JSONParserTest, IntOverBounds) {
    unsigned long long overTheLimit = static_cast<unsigned long long>(LLONG_MAX) + 1;
    
    std::stringstream sstream;
    sstream << overTheLimit;
    std::istringstream inStream("[ " + sstream.str() + " ]");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(-1, result);
    ASSERT_EQ("Unable to parse JSON supplied", parser.getError());
}

TEST(JSONParserTest, IntUnderBounds) {
    unsigned long long overTheLimit = static_cast<unsigned long long>(LLONG_MAX) + 2;
    
    std::stringstream sstream;
    sstream << overTheLimit;
    std::istringstream inStream("[ -" + sstream.str() + " ]");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(-1, result);
    ASSERT_EQ("Unable to parse JSON supplied", parser.getError());
}

TEST(JSONParserTest, InvalidJson) {
    std::istringstream inStream("[ { << {{{{}");

    TestParserCallbacks callbacks;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(-1, result);
    ASSERT_EQ("Unable to parse JSON supplied", parser.getError());
}

TEST(JSONParserTest, FailResponse) {
    std::istringstream inStream("{ }");

    TestParserCallbacks callbacks;
    callbacks.response = 100;
    JSONParser::Parser parser(&callbacks);
    auto result = parser.parseStream(inStream);

    ASSERT_EQ(100, result);
    ASSERT_EQ("", parser.getError());
}