#include "Rectangle.h"

#include <functional>
#include <sstream>

size_t AncestorHash::operator()(const RectanglePtr& rect) const {
    return rect->getAncestorsHash();
}

bool AncestorEqual::operator()(const RectanglePtr& lhs, const RectanglePtr& rhs) const {
    return lhs->ancestorsEqual(rhs);
}

size_t Rectangle::nextNumber = 0u;

Rectangle::Rectangle(unsigned int _x, unsigned int _y, unsigned int _width, unsigned int _height) :
    number(++nextNumber),
    x(_x), y(_y), width(_width), height(_height),
    ancestors(new Set()), ancestorsHash(std::hash<Rectangle*>()(this))
{
}

Rectangle::~Rectangle() {
}

std::string Rectangle::getDescription() const {
    // Realistically, we'd need to consider localistion here, but this is ok for now.
    std::stringstream stringStream;
    stringStream << "(" << x << ", " << y << "), w=" << width << ", h=" << height;
    return stringStream.str();
}

size_t Rectangle::getNumber() const {
    return number;
}

struct AxisComparisonResult {
    AxisComparisonResult() : hasIntersection(false), intersectionCoordinate(0u), intersectionLength(0u) {
    }

    bool hasIntersection;
    unsigned int intersectionCoordinate;
    unsigned int intersectionLength;
};

// Compares the rectangles along a single axis, generating the intersection's co-ordinate and length
// along that axis, if one exists.
static AxisComparisonResult compareAxis(unsigned int coordinate1, unsigned int length1, unsigned int coordinate2, unsigned int length2) {
    AxisComparisonResult result;

    // The first of the 2 rectangles encountered when moving from the origin along the axis.
    unsigned int firstCoordinate;
    unsigned int firstLength;
    unsigned int firstFarSideCoordinate;

    // The second of the 2 rectangles encountered when moving from the origin along the axis.
    unsigned int secondCoordinate;
    unsigned int secondLength;
    unsigned int secondFarSideCoordinate;

    if (coordinate1 < coordinate2) {
        firstCoordinate = coordinate1;
        firstLength = length1;
        firstFarSideCoordinate = coordinate1 + length1; // Warning: this may have wrapped

        secondCoordinate = coordinate2;
        secondLength = length2;
        secondFarSideCoordinate = coordinate2 + length2; // Warning: this may have wrapped
    } else {
        firstCoordinate = coordinate2;
        firstLength = length2;
        firstFarSideCoordinate = coordinate2 + length2; // Warning: this may have wrapped

        secondCoordinate = coordinate1;
        secondLength = length1;
        secondFarSideCoordinate = coordinate1 + length1; // Warning: this may have wrapped
    }

    // In the integer wraparound case (firstFarSideCoordinate < firstCoordinate) there must be an intersection
    // as secondCoordiante is <= UINT_MAX
    if (firstCoordinate <= firstFarSideCoordinate && firstFarSideCoordinate < secondCoordinate) {
        // No overlap on this axis
        result.hasIntersection = false;
    } else {
        // There is an overlap, we need to work out the length
        result.hasIntersection = true;
        result.intersectionCoordinate = secondCoordinate;
        if ((firstCoordinate > firstFarSideCoordinate && secondCoordinate > secondFarSideCoordinate) || 
                (firstCoordinate <= firstFarSideCoordinate && secondCoordinate <= secondFarSideCoordinate)) {
            // Either no integer wraparound has happened, or they both have wrapped around.
            // The result of this will be the same either way (the subtraction will wrap back).
            result.intersectionLength = (secondFarSideCoordinate <= firstFarSideCoordinate ? secondLength : firstFarSideCoordinate - secondCoordinate);
        } else if (firstCoordinate > firstFarSideCoordinate) {
            // Integer wraparound has occured for the first rectangle, but not the second, this means the limit of the
            // intersection is the far side of the second rectangle.
            result.intersectionLength = secondLength;
        } else { // secondCoordinate > secondFarSideCoordinate
            // Integer wraparound has occured for the second rectangle, but not the first, this means the limit of the
            // intersection is the far side of the first rectangle.
            result.intersectionLength = firstFarSideCoordinate - secondCoordinate;
        }
    }

    return result;
}

RectanglePtr Rectangle::getIntersection(const RectanglePtr& other) const {
    RectanglePtr result;
    
    if (other == NULL) {
        return result;
    }

    auto xComparison = compareAxis(x, width, other->x, other->width);
    if (xComparison.hasIntersection) {
        auto yComparison = compareAxis(y, height, other->y, other->height);
        if (yComparison.hasIntersection) {
            result.reset(new Rectangle(xComparison.intersectionCoordinate, yComparison.intersectionCoordinate,
                xComparison.intersectionLength, yComparison.intersectionLength));

            if (ancestors->empty()) {
                result->ancestors->insert(shared_from_this());
            } else {
                result->ancestors->insert(ancestors->begin(), ancestors->end());
            }

            if (other->ancestors->empty()) {
                result->ancestors->insert(other);
            } else {
                result->ancestors->insert(other->ancestors->begin(), other->ancestors->end());
            }

            const_cast<Rectangle*>(result.get())->ancestorsHash = 0u;
            for (auto it = result->ancestors->begin(); it != result->ancestors->end(); ++it) {
                const_cast<Rectangle*>(result.get())->ancestorsHash += (*it)->ancestorsHash;
            }
        }
    }

    return result;
}

const Rectangle::Set::iterator Rectangle::getAncestorsBegin() const {
    return ancestors->begin();
}

const Rectangle::Set::iterator Rectangle::getAncestorsEnd() const {
    return ancestors->end();
}

size_t Rectangle::getAncestorsHash() const {
    return ancestorsHash;
}

bool Rectangle::ancestorsEqual(const RectanglePtr& other) const {
    if (ancestorsHash != other->ancestorsHash) {
        return false;
    }

    if (ancestors->size() != other->ancestors->size()) {
        return false;
    }

    for (auto it = other->ancestors->begin(); it != other->ancestors->end(); ++it) {
        if (ancestors->find(*it) == ancestors->end()) {
            return false;
        }
    }

    return true;
}

bool Rectangle::operator==(const Rectangle& other) const {
    return x == other.x && y == other.y && width == other.width && height == other.height;
}

bool Rectangle::operator!=(const Rectangle& other) const {
    return !operator==(other);
}