#include "InputBuilder.h"

#include "Rectangle.h"

#include "gtest/gtest.h"

// Appends the return char to the string, useful for expecting printed strings
static std::string printedMessage(const std::string& message) {
    std::stringstream sstream;
    sstream << message << std::endl;
    return sstream.str();
}

TEST(InputBuilder, EmptyString) {
    std::istringstream inStream("");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains no rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidJson) {
    std::istringstream inStream("]]]] [[]]]]] {{{]]]]]]]}}}}}}}}} ***((**(*(**(*)))))");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Unable to parse JSON supplied"), errStream.str());
}

TEST(InputBuilder, Array) {
    std::istringstream inStream("[ ]");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input must start with a map"), errStream.str());
}

TEST(InputBuilder, InvalidDictOne) {
    std::istringstream inStream("{ \"one\": 1 }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains no rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidDictTwo) {
    std::istringstream inStream("{ \"rects\": 1 }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains no rectangles"), errStream.str());
}

TEST(InputBuilder, EmptyArray) {
    std::istringstream inStream("{ \"rects\": [ ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains no rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidRectangleOne) {
    std::istringstream inStream("{ \"rects\": [ 1 ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains no rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidRectangleTwo) {
    std::istringstream inStream("{ \"rects\": [ [ ] ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains no rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidRectangleThree) {
    std::istringstream inStream("{ \"rects\": [ { } ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains invalid rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidRectangleFour) {
    std::istringstream inStream("{ \"rects\": [ { \"x\": 0, \"y\": 0, \"w\": 0 } ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains invalid rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidRectangleFive) {
    std::istringstream inStream("{ \"rects\": [ { \"x\": 0, \"y\": 0, \"w\": 0, \"z\": 0 } ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains invalid rectangles"), errStream.str());
}

TEST(InputBuilder, InvalidRectangleSix) {
    std::istringstream inStream("{ \"rects\": [ { \"x\": [] , \"y\": 0, \"w\": 0, \"h\": 0 } ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Input contains invalid rectangles"), errStream.str());
}

TEST(InputBuilder, IntLowBounds) {
    std::istringstream inStream("{ \"rects\": [ { \"x\": -1 , \"y\": 0, \"w\": 0, \"h\": 0 } ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res == NULL);
    ASSERT_EQ(printedMessage("Error: Out of bound integer supplied"), errStream.str());
}

TEST(InputBuilder, ValidRectangles) {
    std::istringstream inStream("{ \"rects\": [ { \"x\": 1 , \"y\": 2, \"w\": 3, \"h\": 4 }, "
                                               "{ \"x\": 2 , \"y\": 4, \"w\": 6, \"h\": 8 }, "
                                               "{ \"x\": 3 , \"y\": 6, \"w\": 9, \"h\": 12 } ] }");
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res != NULL);
    ASSERT_EQ("", errStream.str());

    unsigned int i = 1;
    for (auto it = res->begin(); it != res->end(); ++it, ++i) {
        ASSERT_EQ(Rectangle(i, i*2, i*3, i*4).getDescription(), (*it)->getDescription());
    }
}

TEST(InputBuilder, RectanglesOverflowReported) {
    std::stringstream sstream;
    sstream << "{ \"rects\": [ ";
    for (unsigned int i = 0; i < 1000; i++) {
        sstream << "{ \"x\": 1 , \"y\": 2, \"w\": 3, \"h\": 4 }, ";
    }
    sstream << "{ \"x\": 3 , \"y\": 6, \"w\": 9, \"h\": 12 } ] }"; // The 1001st entry will be ignored

    std::istringstream inStream(sstream.str());
    std::stringstream errStream;

    auto res = InputBuilder::buildInputFromStream(inStream, errStream);

    ASSERT_TRUE(res != NULL);
    ASSERT_EQ(printedMessage("Warning: Too many array entries, some are ignored"), errStream.str());
    ASSERT_EQ(1000, res->size());
    for (auto it = res->begin(); it != res->end(); ++it) {
        ASSERT_EQ(Rectangle(1, 2, 3, 4).getDescription(), (*it)->getDescription());
    }
}
