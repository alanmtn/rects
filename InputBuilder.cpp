#include "InputBuilder.h"

#include "JSONParser.h"
#include "Rectangle.h"

namespace InputBuilder {
    static const size_t MAX_ENTRIES = 1000;

    class InputBuilderJsonCallbacks : public JSONParser::ParserCallback {
    public:
        InputBuilderJsonCallbacks() :
            rectangles(new RectangleList()), 
            dictionaryDepth(0),
            arrayDepth(0),
            gotRectsKey(false),
            rectsDone(false),
            gotX(false),
            x(0),
            gotY(false),
            y(0),
            gotW(false),
            w(0),
            gotH(false),
            h(0) 
        {
        }
        virtual ~InputBuilderJsonCallbacks() {}

        int onDictionaryStart() override {
            dictionaryDepth++;
            if (gotRectsKey && !rectsDone && !inRectangleDict && dictionaryDepth == 2 && arrayDepth == 1) {
                inRectangleDict = true;
            }

            return 0;
        }

    int onDictionaryEnd() override {
        dictionaryDepth--;
        lastKey = "";
        if (inRectangleDict && dictionaryDepth == 1 && arrayDepth == 1) {
            inRectangleDict = false;

            if (!gotX || !gotY || !gotW || !gotH) {
                errorText = "Input contains invalid rectangles";
                return 1;
            } else if (rectangles->size() > MAX_ENTRIES - 1) {
                errorText = "Too many array entries, some are ignored";
            } else {
                rectangles->push_back(RectanglePtr(new Rectangle(x, y, w, h)));
            }

            gotX = gotY = gotW = gotH = false;
        } 

        return 0;
    }

    int onDictionaryKey(const std::string& key) override {
        lastKey = key;
        return 0;
    }

    int onArrayStart() override {
        arrayDepth++;
        if (dictionaryDepth == 0) {
            errorText = "Input must start with a map";
            return 1;
        } else if (dictionaryDepth == 1 && arrayDepth == 1 && lastKey == "rects") {
            gotRectsKey = true;
        }
        return 0;
    }

    int onArrayEnd() override {
        arrayDepth--;
        lastKey = "";

        if (dictionaryDepth == 1 && arrayDepth == 0 && gotRectsKey) {
            rectsDone = true;
        }
        return 0;
    }

    int onInteger(long long integer) override {
        bool usefulNumber = false;
        if (inRectangleDict && dictionaryDepth == 2 && arrayDepth == 1 && !rectsDone) {
            if (lastKey == "x") {
                gotX = true;
                x = integer;
                usefulNumber = true;
            } else if (lastKey == "y") {
                gotY = true;
                y = integer;
                usefulNumber = true;
            } else if (lastKey == "w") {
                gotW = true;
                w = integer;
                usefulNumber = true;
            } else if (lastKey == "h") {
                gotH = true;
                h = integer;
                usefulNumber = true;
            }
        }
        lastKey = "";

        if (usefulNumber && (integer < 0 || integer > UINT_MAX)) {
            errorText = "Out of bound integer supplied";
            return 1;
        }
        return 0;
    }

    std::string getErrorText() {
        return errorText;
    }

    RectangleListPtr getRectangles() {
        return rectangles;
    }

    private:
        RectangleListPtr rectangles;

        size_t dictionaryDepth;
        size_t arrayDepth;

        bool gotRectsKey;
        bool rectsDone;
        bool inRectangleDict;

        std::string lastKey;

        bool gotX;
        unsigned int x;
        bool gotY;
        unsigned int y;
        bool gotW;
        unsigned int w;
        bool gotH;
        unsigned int h;

        std::string errorText;
    };

    RectangleListPtr buildInputFromStream(std::istream& inStream, std::ostream& errStream) {
        InputBuilderJsonCallbacks callbacks;
        JSONParser::Parser parser(&callbacks);
        auto res = parser.parseStream(inStream);

        if (res == -1) {
            errStream << "Error: " << parser.getError() << std::endl;
            return RectangleListPtr();
        } else if (res != 0) {
            errStream << "Error: " << callbacks.getErrorText() << std::endl;
            return RectangleListPtr();
        } else if (callbacks.getRectangles()->empty()) {
            errStream << "Error: Input contains no rectangles" << std::endl;
            return RectangleListPtr();
        }

        if (!callbacks.getErrorText().empty()) {
            errStream << "Warning: " << callbacks.getErrorText() << std::endl;
        }

        return callbacks.getRectangles();
    }
}